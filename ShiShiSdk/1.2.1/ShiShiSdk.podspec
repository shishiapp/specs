Pod::Spec.new do |s|

  s.name         = "ShiShiSdk"
  s.version      = "1.2.1"
  s.summary      = "Pod for ShiShi Virtual Try-On SDK"

  s.homepage     = "http://shishiapp.com"

  s.license    = { :type => "Commercial", :file => "LICENSE" }
  s.authors       = { "Lingyu" => "lingyu@shishiapp.com" }

  s.platform     = :ios

  s.source       = { :git => "git@bitbucket.org:shishiapp/shishisdk-pod.git", :tag => "#{s.version}" }

  s.resources    =   ['visageSDKResources/data/**', 'TryCoreResources/**']

  s.ios.deployment_target = '10.3'

  s.library = 'c++'
  s.xcconfig = {
       'CLANG_CXX_LANGUAGE_STANDARD' => 'c++11',
       'CLANG_CXX_LIBRARY' => 'libc++'
  }

  s.frameworks = "Accelerate", "GLKit", "SceneKit", "OpenGLES", "AVFoundation", "QuartzCore", "MobileCoreServices", "ImageIO", "Foundation", "CoreMedia", "AssetsLibrary"

  s.vendored_frameworks = "ShiShiSdk.framework"

  s.dependency 'RealmSwift', '~> 3.5.0'
  s.dependency 'Alamofire', '~> 4.4'
  s.dependency 'ObjectMapper', '~> 3.3'
  s.dependency 'AlamofireObjectMapper', '~> 5.0'
  s.dependency 'AlamofireImage', '~> 3.2'
  s.dependency 'Kingfisher', '~> 4.0'


end