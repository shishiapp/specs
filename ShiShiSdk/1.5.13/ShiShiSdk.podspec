Pod::Spec.new do |s|

  s.name         = "ShiShiSdk"
  s.version      = "1.5.13"
  s.summary      = "Pod for ShiShi Virtual Try-On SDK"

  s.homepage     = "http://shishiapp.com"

  s.license    = { :type => "Commercial", :file => "LICENSE" }
  s.authors       = { "Lingyu" => "lingyu@shishiapp.com" }

  s.platform     = :ios

  s.source       = { :git => "https://bitbucket.org/shishiapp/shishisdk-pod.git", :tag => "#{s.version}" }

  s.resources    =   ['visageSDKResources/data/**', 'TryCoreResources/**']

  s.ios.deployment_target = '10.3'

  s.library = 'c++'
  s.xcconfig = {
       'CLANG_CXX_LANGUAGE_STANDARD' => 'c++11',
       'CLANG_CXX_LIBRARY' => 'libc++'
  }

  s.frameworks = "Accelerate", "Metal", "SceneKit", "AVFoundation", "QuartzCore", "MobileCoreServices", "ImageIO", "Foundation", "CoreMedia", "AssetsLibrary"

  s.vendored_frameworks = "ShiShiSdk.framework", "ShiShiSdk.framework/Frameworks/TFPlugin.framework"

  s.dependency 'RealmSwift', '~> 5.4'
  s.dependency 'Alamofire', '~> 5.0'
  s.dependency 'ObjectMapper', '~> 3.5'
  s.dependency 'AlamofireImage', '~> 4.0'
  s.dependency 'OpenSSL-Universal', '~> 1.1'


end